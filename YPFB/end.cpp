/* YPFB.cpp : Este archivo hace referencia a os eventos que va a ejecutar el robot en la pantalla de end.
	Developed by Ing Josmar Jim�nez
	josmar66jimenez@gmail.com
*/

#include <string>
#include <windows.h>
#include <iostream>

using namespace std;

void sleep(string);
void do_click(int, int);
int main();
void insert_text(string);

void end(void) {

	string again;
	int closeWindowsPosX = 1338;
	int closeWindowsPosY = 322;
	int exitPosX = 40;
	int exitPosY = 500;

	sleep("long");
	/*salir de pantalla completa*/
	//keybd_event(VK_F11, 0x7A, 0, 0);
	//keybd_event(VK_F11, 0x7A, KEYEVENTF_KEYUP, 0); // Release
	//sleep("short");

	do_click(closeWindowsPosX, closeWindowsPosY);
	sleep("long");
	do_click(exitPosX, exitPosY);

	//sleep("long");
	//keybd_event(VK_MENU, 0x12, 0, 0); // Press
	//keybd_event(VK_TAB, 0x9, 0, 0); // Press
	//keybd_event(VK_TAB, 0x9, KEYEVENTF_KEYUP, 0); // Release
	//keybd_event(VK_MENU, 0x12, KEYEVENTF_KEYUP, 0); // Release
	//sleep("short");

	cout << "Robot ejecutado sastifactoriamente" << endl;
	
	sleep("long");
}