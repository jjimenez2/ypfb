/* YPFB.cpp : Este archivo hace referencia a la funcin sleep que se usa en diferentes lugares del programa.
	Developed by Ing Josmar Jimnez
	josmar66jimenez@gmail.com
*/

#include <string>
#include <windows.h>

using namespace std;

extern int time_short;
extern int time_medium;
extern int time_long;

void sleep(string mode) {

	if (mode == "short")
		Sleep(time_short * 1000);
	else
		if (mode == "medium") {
			Sleep(time_medium * 1000);
		}
		else
		{
			Sleep(time_long * 1000);
		}
}
