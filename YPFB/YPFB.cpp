/* YPFB.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
	Developed by Ing Josmar Jiménez
	josmar66jimenez@gmail.com
*/

#include <windows.h>
#include <iostream>
#include <string>

using namespace std;

/*Definición de métodos*/
void login(void);
void sleep(string);
void do_click(int, int);

/*Definición de variables*/
int time_short = 1;
int time_medium = 2;
int time_long = 4;
string contract = "";
string username = "";
string password = "";


string url	= "https://ypfb-bot-demo-jjjm.web.app/";

/*Main*/
/*Main*/
int main(int argc, char* argv[])
{
	if (argc < 8) {
		std::cerr << "Por favor correr el programa con siete parametros:" << std::endl;
		std::cerr << "1- RUTA DE YPFB LPZ.EXE SIN ESPACIOS Ejemplo: c:\YPFB\LPZ\YPFB-LPZ.exe" << std::endl;
		std::cerr << "2- USUARIO DE LA APLICACI" << char(224) <<"N:"<< std::endl;
		std::cerr << "3- PASSWORD DE LA APLICACI" << char(224) << "N:" << std::endl;
		std::cerr << "4- TIME SHORT" << std::endl;
		std::cerr << "5- TIME MEDIUM" << std::endl;
		std::cerr << "6- TIME LONG" << std::endl;
		std::cerr << "7- YPFB CONTRACT" << std::endl;
		return 1;
	}

	std::cout << "Inicializando robot YPFB versi" << char(162) << "n web 1.0. Favor no realizar ninguna acci" << char(162) << "n\n";

	const char* executable = argv[1];
	username	= argv[2];
	password	= argv[3];
	time_short	= atoi(argv[4]);
	time_medium = atoi(argv[5]);
	time_long	= atoi(argv[6]);
	contract	= argv[7];

	cout << "Ruta del executable: " << executable << endl;
	cout << "Usuario: " << username << endl;
	cout << "Password: " << password << endl;
	cout << "Pausa entre acciones cortas: " << time_short << " segundos" << endl;
	cout << "Pausa entre acciones medias: " << time_medium << " segundos" << endl;
	cout << "Pausa entre acciones largas: " << time_long << " segundos" << endl;
	cout << "Contrato: " << contract << endl;
	
	cout << "Inicializa en 3..." << endl;
	sleep("short");
	cout << "Inicializa en 2..." << endl;
	sleep("short");
	cout << "Inicializa en 1..." << endl;
	sleep("short");

	/*Eliminar esto en producción cuando tengamos los puntos de la pantalla*/
	//std::cout << "Abriendo navegador por defecto\n";
	//ShellExecuteA(GetDesktopWindow(), "open", url.c_str(), NULL, NULL, SW_SHOW);
	//sleep("long");
	/*Colocar el browser en pantalla completa*/
	//keybd_event(VK_F11, 0x7A, 0, 0);
	//keybd_event(VK_F11, 0x7A, KEYEVENTF_KEYUP, 0); // Release
	
	//system(executable);
	ShellExecuteA(NULL, "open", executable, NULL, NULL, SW_SHOWDEFAULT);

	//keybd_event(VK_MENU, 0x12, 0, 0); // Press
	//keybd_event(VK_TAB, 0x9, 0, 0); // Press
	//keybd_event(VK_TAB, 0x9, KEYEVENTF_KEYUP, 0); // Release
	//keybd_event(VK_MENU, 0x12, KEYEVENTF_KEYUP, 0); // Release

	sleep("long");
	sleep("long");

	/*Ir a Login*/
	login();
	return 0;
}
