/* YPFB.cpp : Este archivo hace referencia a os eventos que va a ejecutar el robot en la pantalla de login.
	Developed by Ing Josmar Jim�nez
	josmar66jimenez@gmail.com
*/

#include <string>
#include <windows.h>
#include <iostream>

using namespace std;

void home(void);
void sleep(string );
void do_click(int, int);
void insert_text(string);

void login(void) {

	sleep("long");

	int userPosX = 1190;
	int userPosY = 490;
	int passwordPosX = 1190;
	int passwordPosY = 513;
	int buttonEnterPosX = 1144;
	int buttonEnterPosY = 590;

	extern string	username;
	extern string	password;

	// Ejecutando click en usuario
	do_click(userPosX, userPosY);
	sleep("short");
	
	insert_text(username);
	sleep("short");

	do_click(passwordPosX, passwordPosY);

	// Ejecutando click en password
	insert_text(password);
	
	sleep("long");

	do_click(buttonEnterPosX, buttonEnterPosY);
	home();
}